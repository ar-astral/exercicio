<?php
namespace Drupal\kinship;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

class PersonStorage extends SqlContentEntityStorage
{
    public function delete(array $entities, bool $force = false)
    {
        parent::delete($entities);
    }
}