<?php

namespace Drupal\kinship\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\kinship\PersonInterface;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\Tests\EntityViewTrait;
use Drupal\user\EntityOwnerTrait;
use Robo\Config;

/**
 * Defines the person entity class.
 *
 * @ContentEntityType(
 *   id = "person",
 *   label = @Translation("Person"),
 *   label_collection = @Translation("Persons"),
 *   label_singular = @Translation("person"),
 *   label_plural = @Translation("persons"),
 *   label_count = @PluralTranslation(
 *     singular = "@count persons",
 *     plural = "@count persons",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\kinship\PersonStorage",
 *     "list_builder" = "Drupal\kinship\PersonListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\kinship\Form\PersonForm",
 *       "edit" = "Drupal\kinship\Form\PersonForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "person",
 *   admin_permission = "administer person",
 *   entity_keys = {
 *     "id" = "id",
 *     "name" = "name",
 *     "age" = "age"
 *   },
 *   links = {
 *     "collection" = "/admin/content/person",
 *     "add-form" = "/person/add",
 *     "canonical" = "/person/{person}",
 *     "edit-form" = "/person/{person}/edit",
 *     "delete-form" = "/person/{person}/delete",
 *   },
 *   field_ui_base_route = "entity.person.settings",
 * )
 */
class Person extends ContentEntityBase implements PersonInterface {
  
  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage)
  {
    $field_avatar = $this->get('avatar');
    //Add new avatar when new instance is persisted and non default image is provided.
    if ($this->isNew() && !$field_avatar->getValue())
    { 
      /**
        * @var \Drupal\file\FileInterface
        */
      $avatar = \Drupal::service('kinship.avatar')->fetchAvatar(); 
      $field_avatar->setValue(
        [
          'target_id' => $avatar->id(),
          'title' => 'avatar',
          'alt' => 'avatar',
          'width' => 100, //defaut api size
          'height' => 100 //defaul api size
        ]
      );
    }
  }

  public function delete()
  {
    //Implement soft delete
    $this->set('status', false)->save();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);
      
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setSetting('min_length', 5)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);
      
    $fields['age'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Age'))
      ->setDescription(t('The Age of the Person.'))
      ->setSetting('unsigned', TRUE)
      ->setRequired(true)
      ->setDisplayOptions('form', [
        'label' => 'hidden',
        'type' => 'number',
        'min' => 0,
        'max' => 99,
        'weight' => -4,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'number_integer',
        'weight' => -5,
      ]);
    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);
      $fields['kinships'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Kinships'))
      ->setSetting('target_type', 'kinship')
      ->setCardinality(-1)
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'settings' => [    
          'form_mode' => 'default',
          'override_labels' => false,
          'label_singular' => '',
          'label_plural' => '',
          'allow_new' => true,
          'allow_existing' => false,
          'match_operator' => 'CONTAINS',
          'allow_duplicate' => false,
          'collapsible' => false,
          'collapsed' => true,
          'revision' => false
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_entity_view',
        'settings' => [    
          'view_mode' => 'default',
          'link' => true,
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

      $fields['avatar'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Avatar'))
      ->setRequired(false)
      ->setSettings(
        [
        'handler' => 'default:file',
        'handler_settings' => [],
        'file_directory' => 'avatar',
        'file_extensions' => 'png gif jpg jpeg',
        'max_filesize' => '',
        'max_resolution' => '300x300',
        'min_resolution' => '',
        'alt_field' => true,
        'alt_field_required' => false,
        'title_field' => false,
        'title_field_required' => false,
        'default_image' => 
          [
            'uuid' => '',
            'alt' => 'Avatar',
            'title' => 'Avatar',
            'width' => null,
            'height' => null
          ]
      ])
      ->setDisplayOptions('form', 
        [
          'type' => 'image_image',
          'settings' =>
            [
              'progress_indicator' => 'throbber',
              'preview_image_style' => 'thumbnail'
            ]
        ])
      ->setDisplayOptions('view',
        [
          'type' => 'image',
          'settings' =>
            [
              'image_link' => '',
              'image_style' => '',
              'image_loading' => ['attribute' => 'lazy']
            ]
            ]);

    return $fields;
  }

  public function label(): string
  {
    return $this->__toString();
  }

  public function __toString(): string
  {
    return $this->get('name')->value . ' (' . $this->get('age')->value .')';
  }
}
