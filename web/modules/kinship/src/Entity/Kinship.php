<?php

namespace Drupal\kinship\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\kinship\KinshipInterface;

/**
 * Defines the kinship entity class.
 *
 * @ContentEntityType(
 *   id = "kinship",
 *   label = @Translation("Kinship"),
 *   label_collection = @Translation("Kinships"),
 *   label_singular = @Translation("kinship"),
 *   label_plural = @Translation("kinships"),
 *   label_count = @PluralTranslation(
 *     singular = "@count kinships",
 *     plural = "@count kinships",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\kinship\KinshipListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\kinship\Form\KinshipForm",
 *       "edit" = "Drupal\kinship\Form\KinshipForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "kinship",
 *   admin_permission = "administer kinship",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/kinship",
 *     "add-form" = "/kinship/add",
 *     "canonical" = "/kinship/{kinship}",
 *     "edit-form" = "/kinship/{kinship}/edit",
 *     "delete-form" = "/kinship/{kinship}/delete",
 *   },
 *   field_ui_base_route = "entity.kinship.settings",
 * )
 */
class Kinship extends ContentEntityBase implements KinshipInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }
  public function delete() {
    //Implement soft delete
    $this->set('status', false)->save();
  }
}
