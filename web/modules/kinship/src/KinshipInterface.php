<?php

namespace Drupal\kinship;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a kinship entity type.
 */
interface KinshipInterface extends ContentEntityInterface {

}
