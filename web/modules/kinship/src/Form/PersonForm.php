<?php

namespace Drupal\kinship\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the person entity edit forms.
 */
class PersonForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New person %label has been created.', $message_arguments));
        $this->logger('kinship')->notice('Created new person %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The person %label has been updated.', $message_arguments));
        $this->logger('kinship')->notice('Updated person %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.person.canonical', ['person' => $entity->id()]);

    return $result;
  }

}
