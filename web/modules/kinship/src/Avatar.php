<?php

namespace Drupal\kinship;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\file\FileInterface;

/**
 * Service description.
 */
class Avatar {

  const API_URL = 'https://app.pixelencounter.com/api/basic/monsters/random/png';
  const AVATAR_FOLDER = 'avatar';

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $file_system;

  /**
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $file_repository;
  public function __construct(FileSystemInterface $file_system, FileRepositoryInterface $file_repository)
  {
    $this->file_system = $file_system;
    $this->file_repository = $file_repository;
  }

  public function fetchAvatar(): FileInterface
  {
    $destination = 'public://' . self::AVATAR_FOLDER ;
    $this->file_system->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY);
    $data = file_get_contents(self::API_URL);
    $name = md5($data) . '.png';
    $destination .= '/' . $name;
    return $this->file_repository->writeData($data, $destination, FileSystemInterface::EXISTS_REPLACE);
  }
}
