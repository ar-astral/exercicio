<?php

/**
 * @file
 * Provides a person entity type.
 */

use Drupal\Core\Render\Element;
use Drupal\kinship\Entity\Person;
use Drupal\user\UserInterface;

/**
 * Implements hook_theme().
 */
function kinship_theme() {
  return [
    'person' => [
      'render element' => 'elements',
    ],
  ];
}

/**
 * Prepares variables for person templates.
 *
 * Default template: person.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the person information and any
 *     fields attached to the entity.
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_person(array &$variables) {
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
 * Prepares variables for kinship templates.
 *
 * Default template: kinship.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the kinship information and any
 *     fields attached to the entity.
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_kinship(array &$variables) {
  $variables['view_mode'] = $variables['elements']['#view_mode'];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
