# Exercicio

Kinship Management
Plugin


Develop a Drupal plugin to manage people and kinships

Application Features

The plugin should have the following features:


Plugin management area / backoffice / administration

1 - A page for listing people.

2 - A page to add a new person or edit an existing person.

3 - A page to add a new kinship relation to a person.

4 - A page to view the details of a person.

5 - Allow deletion of a person.

6 - Allow deletion of a kinship relation.


Plugin public area

1 - A page for listing people with filtering.



Considerations
There will be 2 entity types in the platform, person and kinship, described below.

The public page for listing people should allow filtering by name and kinship. Adding, editing and deleting people and kinships should only be available through the backoffice.
Person

A person is an entity with 3 fields: ID, name and age. Name should be a string of any size greater than 5 and age should be a valid number.

Each row in the person list page should have a link / button to create a kinship for the person, another to edit the person and another to delete the person. The delete should perform a soft delete of the record, keeping the record in the database but not visible.

Kinship

A person can have any number of kinship relations. The available kinship relation types are: mother, father, son, daughter, brother, sister.

In the person details page, new kinship relations can be added, by selecting a kinship from a dropdown and another dropdown for choosing an existing person in the system to add as kin.

In the person details page, there will be a list of kinship relations regarding that person. The kinship relation list includes the type, person name and age.

Each of the kinships in the person details page should have a link or button for editing and deleting the kinship.

Name and age should be unique within the system, there cannot be multiple people with the same tuple (Name + Age).

Additional Requirements
The following requirements should be implemented if within test execution time:

-Add an icon / avatar to each person. Use the following API to generate an image when the person is created. Store that image within the database, and show it in the people list and on the person details page:
https://app.pixelencounter.com/api/basic/monsters/random

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ar-astral/exercicio.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/ar-astral/exercicio/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Clone the repository to a local environment.
Install PHP dependencies using composer.
Install Drupal project using drush site:install or accessing the Drupal's installer throw a web browser.
Enable the Kinship module

On a terminal run:
git clone https://www.gitlab.com/ar-astral/exercicio
cd exercicio
composer install
./bin/drush site:install (Or access the Drupal's Installer throw  a web browser).
./bin/vendor pm:install kinship (Or enable it via Drupal's web browser interface at  http://<drupalsite>/admin/modules).
## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
